/*
Copyright © 2022 Robert Lützner

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
package models

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path"
)

const packagesJsonFile = ".gopama.pkgs.json"

type Packages struct {
	Version  int       `json:"version"`
	Packages []Package `json:"packages"`
}

type Package struct {
	Name    string `json:"name"`
	Version string `json:"version"`
}

func InitOrLoad() (*Packages, error) {
	packagesFile, err := getPackagesJsonFile()
	if err != nil {
		return nil, err
	}

	fileExists, err := checkIfFileExists(packagesFile)
	if err != nil {
		return nil, fmt.Errorf("unable to stat packages json file: %s", err)
	}
	if !fileExists {
		newPackages := initNewPackages()
		return newPackages, nil
	}

	packages, err := parseFile(packagesFile)
	if err != nil {
		return nil, fmt.Errorf("unable to read packages json file: %s", err)
	}
	return packages, nil
}

func getPackagesJsonFile() (string, error) {
	home, err := os.UserHomeDir()
	if err != nil {
		return "", fmt.Errorf("unable to determine home dir: %s", err)
	}
	packagesFile := path.Join(home, packagesJsonFile)
	return packagesFile, nil
}

func checkIfFileExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err != nil && errors.Is(err, os.ErrNotExist) {
		return false, nil
	}
	return true, err
}

func initNewPackages() *Packages {
	newPackages := &Packages{
		Version: 1,
	}
	return newPackages
}

func parseFile(packagesFile string) (*Packages, error) {
	bytes, err := ioutil.ReadFile(packagesFile)
	if err != nil {
		return nil, err
	}
	var packages Packages
	err = json.Unmarshal(bytes, &packages)
	return &packages, err
}

func (p *Packages) save() error {
	packagesFile, err := getPackagesJsonFile()
	if err != nil {
		return err
	}
	bytes, err := json.Marshal(p)
	if err != nil {
		return fmt.Errorf("error converting to json: %s", err)
	}
	err = os.WriteFile(packagesFile, bytes, 0644)
	if err != nil {
		return fmt.Errorf("error while writing packages json file: %s", err)
	}
	return nil
}

func (p *Packages) Add(newPackage Package) error {
	p.Packages = append(p.Packages, newPackage)
	return p.save()
}

/*
Copyright © 2022 Robert Lützner

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
package cmd

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"strings"

	"codeberg.org/rluetzner/gopama/models"
	"github.com/spf13/cobra"
)

// installCmd represents the install command
var installCmd = &cobra.Command{
	Use:   "install",
	Short: "Install a new go tool",
	Long:  `Install a new go tool.`,
	Run:   runInstall,
}

func init() {
	rootCmd.AddCommand(installCmd)
}

func runInstall(cmd *cobra.Command, args []string) {
	checkArguments(args)
	packages := getPackages()
	installTools(args, packages)
}

func checkArguments(args []string) {
	if len(args) < 1 {
		log.Fatalln("You have to enter at least one package.")
	}
}

func getPackages() *models.Packages {
	packages, err := models.InitOrLoad()
	if err != nil {
		log.Fatalln(err)
	}
	return packages
}

func installTools(args []string, packages *models.Packages) {
	for _, arg := range args {
		pkgName, pkgVersion, err := getPackageNameAndVersion(arg)
		if err != nil {
			log.Fatalln(err)
		}

		alreadyInstalled := checkIfPkgIsAlreadyInstalled(packages, pkgName)
		if alreadyInstalled {
			fmt.Printf("Tool %s is already installed.\n", arg)
			continue
		}

		fmt.Printf("Installing %s...\n", arg)
		err = installTool(arg)
		if err != nil {
			log.Fatalf("Error installing %s: %s\n", arg, err)
		}

		if err = addNewPackage(packages, pkgName, pkgVersion); err != nil {
			log.Fatalln(err)
		}
	}
}

func getPackageNameAndVersion(arg string) (string, string, error) {
	split := strings.Split(arg, "@")
	if len(split) != 2 {
		return "", "", fmt.Errorf("unable to determine package version: %s", arg)
	}
	pkgName := split[0]
	pkgVersion := split[1]
	return pkgName, pkgVersion, nil
}

func checkIfPkgIsAlreadyInstalled(packages *models.Packages, pkgName string) bool {
	alreadyInstalled := false
	for _, p := range packages.Packages {
		if p.Name == pkgName {
			alreadyInstalled = true
			break
		}
	}
	return alreadyInstalled
}

func installTool(arg string) error {
	cmd := exec.Command("go", "install", arg)
	cmd.Stdout = os.Stdout
	cmd.Stdin = os.Stdin
	return cmd.Run()
}

func addNewPackage(packages *models.Packages, pkgName string, pkgVersion string) error {
	newPackage := &models.Package{
		Name:    pkgName,
		Version: pkgVersion,
	}
	return packages.Add(*newPackage)
}

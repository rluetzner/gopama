/*
Copyright © 2022 Robert Lützner

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
package cmd

import (
	"fmt"
	"log"
	"os"
	"os/exec"

	"codeberg.org/rluetzner/gopama/models"
	"github.com/spf13/cobra"
)

// updateCmd represents the update command
var updateCmd = &cobra.Command{
	Use:   "update",
	Short: "Update all packages",
	Long:  `Update all packages.`,
	Run:   runUpdate,
}

func init() {
	rootCmd.AddCommand(updateCmd)
}

func runUpdate(cmd *cobra.Command, args []string) {
	packages := getPackages()
	updatePackages(packages)
}

func updatePackages(packages *models.Packages) {
	for _, p := range packages.Packages {
		if p.Version != "latest" {
			fmt.Printf("Skipping %s. Version is pinned to %s.\n", p.Name, p.Version)
			continue
		}
		fmt.Printf("Updating %s...\n", p.Name)
		if err := updatePackage(p); err != nil {
			log.Fatalf("Error updating %s: %s\n", p.Name, err)
		}
	}
}

func updatePackage(p models.Package) error {
	cmd := exec.Command("go", "install", fmt.Sprintf("%s@latest", p.Name))
	cmd.Stdout = os.Stdout
	cmd.Stdin = os.Stdin
	return cmd.Run()
}

# gopama - A package manager for tools written in Go

## Getting started

```bash
# Install the tool
go install codeberg.org/rluetzner/gopama@latest

# Add gopama to the list of managed tools
gopama install codeberg.org/rluetzner/gopama@latest

# Install another tool
gopama install github.com/spf13/cobra-cli@latest

# Update all your installed tools
gopama update
```
